package az.allstProject.get.controller;

import az.allstProject.get.dto.StudentDto;
import az.allstProject.get.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService service;

    @PostMapping
    public StudentDto createStudent(@RequestBody StudentDto dto) {
        return service.createStudent(dto);
    }

    @GetMapping("/{id}")
    public StudentDto getStudentById(@PathVariable Long id) {
        return service.getStudentById(id);
    }

    @GetMapping()
    public List<StudentDto> getAll() {
        return service.findAllStudent();
    }


    @PutMapping
    public StudentDto updateStudent(@RequestBody StudentDto dto) {
        return service.updateStudent(dto);
    }

    @DeleteMapping("/{id}")
    void deleteStudentById(@PathVariable Long id) {
        service.deleteStudentById(id);
    }
}
